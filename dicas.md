# Dicas SSH

### Matar o processo que roda na porta 3333

\$ lsof -i :3333

- Pegar o pid e digitar
  \$ kill -9 <pid>

### Não deixar a conexão ssh dê timeout

\$ sudo vim /etc/ssh/sshd_config

- Incluir o trecho:

```bash
ClientAliveInterval 30
TCPKeepAlive yes
ClientAliveCountMax 999999
```

\$ sudo service sshd restart
