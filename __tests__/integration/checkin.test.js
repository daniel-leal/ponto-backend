import request from 'supertest';

import app from '../../src/app';
import truncate from '../util/truncate';
import factory from '../factories';

describe('Employee Checkin', () => {
  beforeEach(async () => {
    await truncate();
  });

  it('Should be able to checkin with dleal credentials', async () => {
    const response = await request(app)
      .post('/bater-ponto')
      .send({ loginAD: 'dleal', senha: '12345678' });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
  });

  it('Should be able to checkin with hcjunior credentials', async () => {
    const response = await request(app)
      .post('/bater-ponto')
      .send({ loginAD: 'hcjunior', senha: '12345678' });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
  });

  it('Should not be able to checkin with invalid credentials', async () => {
    const employeeCredential = await factory.attrs('employeeCredential');

    const response = await request(app)
      .post('/bater-ponto')
      .send(employeeCredential);

    expect(response.status).toBe(401);
  });

  it('Should be able to list records', async () => {
    const response = await request(app)
      .post('/demonstrativo')
      .send({ loginAD: 'dleal', senha: '12345678' });

    expect(response.status).toBe(200);
  });
});
