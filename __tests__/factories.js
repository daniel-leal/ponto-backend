import faker from 'faker';
import { factory } from 'factory-girl';

import Checkin from '../src/app/models/Checkin';

factory.define('employeeCredential', Checkin, {
  loginAD: faker.internet.userName(),
  senha: faker.internet.password(),
});

export default factory;
