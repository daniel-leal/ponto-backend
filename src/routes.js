import { Router } from 'express';

// Controllers
import CheckinController from './app/controllers/CheckinController';

// Middlewares
import authMiddleware from './app/middlewares/auth';

const routes = new Router();

/**
 * API Check
 */
routes.get('/', async (req, res) => {
  res.json({
    status: 'ok',
    version: '1.0.0',
    app: 'PontoE',
    mode: process.env.NODE_ENV,
  });
});

routes.get('/jenkins', (req, res) => {
  return res.send('ESTEIRA DEVOPS OK');
});

/**
 * Logged users
 */
routes.use(authMiddleware);

/**
 * Checkin Controller
 */
routes.post('/demonstrativo', CheckinController.index);
routes.post('/bater-ponto', CheckinController.store);

export default routes;
