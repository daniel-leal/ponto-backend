export default async (req, res, next) => {
  const { loginAD } = req.body;

  if (loginAD === 'dleal') {
    req.body.idFuncionario = '52841';
    return next();
  }
  if (loginAD === 'hcjunior') {
    req.body.idFuncionario = '54798';
    return next();
  }
  return res.status(401).json({
    error: 'Ocorreu um erro:',
    messages: 'Usuário ou Senha inválidos',
  });
};
