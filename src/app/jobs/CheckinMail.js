import { format, parseISO } from 'date-fns';
import pt from 'date-fns/locale/pt';

import Mail from '../../lib/Mail';

class CheckinMail {
  get key() {
    return 'CheckinMail';
  }

  async handle({ data }) {
    const { loginAD, idFuncionario, ip, dataHora } = data;

    await Mail.sendMail({
      to: `${loginAD}@banparanet.com.br`,
      subject: 'Batida de Ponto',
      template: 'Checkin',
      context: {
        loginAD,
        idFuncionario,
        ip,
        dataHora: format(
          parseISO(dataHora),
          "'dia' dd 'de' MMMM 'de' yyyy 'às' hh:MM:ss'",
          {
            locale: pt,
          }
        ),
      },
    });
  }
}

export default new CheckinMail();
