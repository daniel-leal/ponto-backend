import Sequelize, { Model } from 'sequelize';

class Checkin extends Model {
  static init(sequelize) {
    super.init(
      {
        idFuncionario: Sequelize.STRING,
        dataHora: Sequelize.DATE,
        ip: Sequelize.STRING,
        loginAD: Sequelize.STRING,
        canal: Sequelize.INTEGER,
        checksum: Sequelize.INTEGER,
      },
      {
        sequelize,
        tableName: 'TBatida',
      }
    );

    return this;
  }
}

export default Checkin;
