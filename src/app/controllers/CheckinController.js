import { Op } from 'sequelize';
import { format, addDays } from 'date-fns';

import Checkin from '../models/Checkin';
// import Queue from '../../lib/Queue';
// import CheckinMail from '../jobs/CheckinMail';

class CheckinController {
  async index(req, res) {
    const checkins = await Checkin.findAll({
      where: {
        [Op.and]: [
          { loginAD: req.body.loginAD },
          {
            dataHora: {
              [Op.gt]: format(new Date(), 'yyyy-MM-dd'),
              [Op.lt]: addDays(new Date(), 1),
            },
          },
        ],
      },
      order: [['dataHora', 'ASC']],
    });

    return res.json(checkins);
  }

  async store(req, res) {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    const checkin = await Checkin.create({
      ...req.body,
      ip,
      dataHora: new Date(),
    });

    // const { loginAD, idFuncionario, dataHora } = checkin;

    // Job de envio de e-mail
    // await Queue.add(CheckinMail.key, { loginAD, idFuncionario, ip, dataHora });

    return res.status(201).json(checkin);
  }
}

export default new CheckinController();
