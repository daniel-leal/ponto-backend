module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('TBatida', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      idFuncionario: {
        type: Sequelize.STRING(16),
      },
      dataHora: {
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
      ip: {
        type: Sequelize.STRING(15),
      },
      loginAD: {
        type: Sequelize.STRING(50),
      },
      canal: {
        type: Sequelize.INTEGER,
      },
      checksum: {
        type: Sequelize.INTEGER,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('TBatida');
  },
};
